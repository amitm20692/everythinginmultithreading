package everythinginmultithreading;

public class Assignment1 {
	public static void main(String[] args) {
		Thread t1 = new Thread (()-> {
			threadStartFunnction();
		});
		t1.start();
		System.out.println("I have started Thread");
	}

	private static void threadStartFunnction() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Local :"+i);
		}
		
	}
}
